from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .views import *

# Create your tests here.
class test(TestCase):
    def test_url_exist(self):
        response = Client().get('/book')
        self.assertEqual(response.status_code,200)
    def test_form_template_used(self):
        response = Client().get('/book')
        self.assertTemplateUsed(response, 'book.html')
    def test_event_url_is_exist(self):
        response = Client().get('/book/cari?q=book')
        self.assertEqual(response.status_code, 200)
