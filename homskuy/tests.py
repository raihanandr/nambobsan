from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .views import *

# Create your tests here.
class test(TestCase):
    def test_url_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)
    def test_form_template_used(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'index.html')
