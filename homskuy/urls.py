from django.urls import path

from . import views

app_name = 'homskuy'

urlpatterns = [
    path('', views.index, name='index'),]